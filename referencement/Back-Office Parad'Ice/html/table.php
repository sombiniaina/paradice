<?php
	require('connexionmysql.php');
	require('model.php');
	$connexion=Mysql_connection();
	$findProduit=find_table("Produit", "", $connexion);
	$findCategorie=find_table("Categorie", "", $connexion);
	$findParfum=find_table("Parfum", "", $connexion);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Back-Office Parad'Ice</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="index.php">
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon--><img src="../plugins/images/admin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="../plugins/images/admin-logo-dark.png" alt="home" class="light-logo" />
                     </b>
                        <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text--><img src="../plugins/images/admin-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="../plugins/images/admin-text-dark.png" alt="home" class="light-logo" />
                     </span> </a>
                </div>
                <!-- /Logo -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                   
                    <li>
                        <a class="profile-pic" href="#"> <b class="hidden-xs">Admin</b></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
                </div>
                <ul class="nav" id="side-menu">
				   <li>
                        <a href="table.php" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i>Tous les produits</a>
                    </li>
                    <li>
                        <a href="table.php" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i>Tous les produits</a>
                    </li>
                  
                    <li>
                        <a href="insertProduit.php" class="waves-effect"><i class="fa fa-columns fa-fw" aria-hidden="true"></i>Insertion produit</a>
                    </li>
					
                   
                </ul>
                <div class="center p-20">
                     <a href="insertProduit.php" target="_blank" class="btn btn-danger btn-block waves-effect waves-light">Insert produit</a>
                 </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Tous les produits</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <a href="insertProduit.php" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">insert produit

                        </a>
                        <ol class="breadcrumb">
             
                            <li class="active">Table produit</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Tous les produits disponibles chez parad'Ice</h3>
                            
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>IdCategorie</th>
                                            <th>IdParfum</th>
                                            <th>Image</th>
                                            <th>Type</th>
											<th>Nom</th>
                                            <th>description</th>
                                            <th>prix</th>
                                            <th>---</th>
                                            <th>---</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php foreach($findProduit AS $prod){ ?>
                                        <tr>
                                            <td><?php echo $prod->id ?></td>
                                            <td><?php echo $prod->idCategorie ?></td>
                                            <td><?php echo $prod->idParfum ?></td>
                                            <td><img src="<?php echo $prod-> referencementImage ?>" ?></td>
                                            <td><?php echo $prod->image ?></td>
                                            <td><?php echo $prod->nom ?></td>
                                            <td><?php echo $prod->description ?></td>
                                            <td><?php echo $prod->prix ?></td>
                                            <td><a href="forms.php?id=<?php echo $prod->id?>&image=<?php echo $prod->referencementImage ?>&nom=<?php echo $prod->nom ?>&description=<?php echo $prod->description ?>&prix=<?php echo $prod->prix ?>&type=<?php echo $prod->image ?>"><button class="btn btn-success">modifier</button></td>
								            <td><a href="supprimerProduit.php?id=<?php echo $prod->id?>"><button class="btn btn-danger">supprimer</button></td>
                                        </tr>
									<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Tous les categories de produits disponibles chez parad'Ice</h3>
                            
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>typeCategorie</th>
                                            <th>---</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php foreach($findCategorie AS $cat){ ?>
                                        <tr>
                                            <td><?php echo $cat->id ?></td>
                                            <td><?php echo $cat->typeCategorie ?></td>
								            <td><a href="supprimerCategorie.php?id=<?php echo $cat->id?>"><button class="btn btn-danger">supprimer</button></td>
                                        </tr>
									<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Tous les parfums de produits disponibles chez parad'Ice</h3>
                            
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>typeParfum</th>
                                            <th>---</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php foreach($findParfum AS $parf){ ?>
                                        <tr>
                                            <td><?php echo $parf->id ?></td>
                                            <td><?php echo $parf->typeParfum ?></td>
								            <td><a href="supprimerParfum.php?id=<?php echo $parf->id?>"><button class="btn btn-danger">supprimer</button></td>
                                        </tr>
									<?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2018 &copy; parad'Ice.com</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
</body>

</html>
