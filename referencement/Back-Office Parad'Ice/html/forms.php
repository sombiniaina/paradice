<?php
	require('model.php');
	require('connexionmysql.php');
	$connexion=Mysql_connection();
	$id=$_GET["id"];
	$image=$_GET["image"];
	echo $image;
	$nom=$_GET["nom"];
	echo $nom;
	$prix=$_GET["prix"];
	$description=$_GET["description"];
	echo $description;
	$type=$_GET["type"];
	echo $type;
	$findCategorie=find_table("Categorie", "", $connexion);
	$findParfum=find_table("Parfum", "", $connexion);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>Back-Office Parad'Ice</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="table.html">
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon--><img src="../plugins/images/admin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="../plugins/images/admin-logo-dark.png" alt="home" class="light-logo" />
                     </b>
                        <!-- Logo text image you can use text also --><span class="hidden-xs">
                        <!--This is dark logo text--><img src="../plugins/images/admin-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="../plugins/images/admin-text-dark.png" alt="home" class="light-logo" />
                     </span> </a>
                </div>
                <!-- /Logo -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                 
                    <li>
                        <a class="profile-pic" href="#"><b class="hidden-xs">Admin</b></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3>
                </div>
                <ul class="nav" id="side-menu">
                    
                    <li>
                        <a href="profile.html" class="waves-effect"><i class="fa fa-user fa-fw" aria-hidden="true"></i>Administrateur</a>
                    </li>
                    <li>
                        <a href="table.php" class="waves-effect"><i class="fa fa-table fa-fw" aria-hidden="true"></i>Tous les produits</a>
                    </li>
                  
                    <li>
                        <a href="insertProduit.php" class="waves-effect"><i class="fa fa-columns fa-fw" aria-hidden="true"></i>Insertion produit</a>
                    </li>
					
                   
                </ul>
                <div class="center p-20">
                     <a href="insertProduit.php" target="_blank" class="btn btn-danger btn-block waves-effect waves-light">Insert produit</a>
                 </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Formulaire de modification</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        
                        <ol class="breadcrumb">
                           
                            <li class="active">modifier produit</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                <div class="col-md-12">

	  			<div class="row">
	  				<div class="col-md-6">
	  					<div class="content-box-large">
			  				<div class="panel-heading">
					            <div class="panel-title">Veuillez faire une modification</div>
					          
					           
					        </div>
							
							
			  				<div class="panel-body">
			  					<form class="form-horizontal" action="modifierProduit.php" method="post" enctype="multipart/form-data">
								<div class="form-group">
										<div class="form-group">
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-2 control-label">Id</label>
											<div class="col-sm-10">
												<input type="text" name="id" value="<?php echo $id ?>" class="form-control" id="inputEmail3" placeholder="nom">
											</div>
										</div>
										<label for="inputEmail3" class="col-sm-2 control-label">Categorie</label>
											<div class="col-sm-10">
												 <select name="idCat" >
													<?php foreach($findCategorie AS $cat){ ?>
				                                        <option  name="idCat"  value="<?php echo $cat->id ?>" ><?php echo $cat->typeCategorie ?></option>
								                    <?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
										<label for="inputEmail3" class="col-sm-2 control-label">Parfum</label>
											<div class="col-sm-10">
												 <select name="idParf" >
													<?php foreach($findParfum AS $parf){ ?>
				                                        <option  name="idParf"  value="<?php echo $parf->id ?>" ><?php echo $parf->typeParfum ?></option>
								                    <?php } ?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-2 control-label">nom...</label>
											<div class="col-sm-10">
												<input type="text" name="nom" value="<?php echo $nom ?>" class="form-control" id="inputEmail3" placeholder="nom">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">description</label>
											<div class="col-sm-10">
												<textarea name="description" value="<?php echo $description ?>" class="form-control" placeholder="description..." rows="3"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-2 control-label">prix</label>
											<div class="col-sm-10">
												<input type="text" name="prix" value="<?php echo $prix ?>" class="form-control" id="inputEmail3" placeholder="prix">
											</div>
										</div>
 										<div class="form-group">
											
												Select image to upload (JPG only):
												<input type="file" name="fileToUpload" id="fileToUpload">
												New name(without extension):
												<input type="text" name="image" id="image">
												
											
										</div>
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-2 control-label">Type</label>
											<div class="col-sm-10">
												<input type="text" name="type" value="<?php echo $type ?>" class="form-control" id="inputEmail3" placeholder="prix">
											</div>
										</div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <button type="submit" class="btn btn-primary">Modifier</button>
								    </div>
								  </div>
								</form>
			  				</div>
			  			</div>
	  				</div>
	  				

	  		<!--  Page content -->
		  </div>
		</div>
    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2017 &copy; Ample Admin brought to you by wrappixel.com </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
</body>

</html>
