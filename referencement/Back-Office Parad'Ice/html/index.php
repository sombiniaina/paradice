<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap Colored Login Form Template</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Veuillez</strong> remplir la formulaire pour que vous puissez vous connecter </h1>
							
                            <div class="description">
                            	<p> 
	                            	<a href="http://azmind.com"><strong></strong></a>
                            	</p>
                            </div>
						
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login to our site</h3>
									<?php if(isset($_GET['erreur']) && $_GET['erreur']==1){ ?>
									<p style="color:#A52A2A"> 
			&nbsp;&nbsp;D&eacute;sol&eacute;! Il y a eu un erreur lors de la connexion!
			Il y a peut-&ecirc;tre une faute &agrave; l'insertion du login ou du mot de passe.
			Veuillez vous reconnecter!</br> 
		</p>
	<?php } ?>
                            		<p>Enter votre coordonner:</p>
									
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="verificationlogin.php" method="post" >
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">nom</label>
			                        	<input type="text" name="nom" placeholder="nom..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">prenom</label>
			                        	<input type="text" name="prenom" placeholder="prenom..." class="form-prenom form-control" id="form-prenom">
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only" for="form-password">mail</label>
			                        	<input type="text" name="mail" placeholder="mail..." class="form-mail form-control" id="form-mail">
			                        </div>
									<div class="form-group">
			                        	<label class="sr-only" for="form-password">mot de passe</label>
			                        	<input type="password" name="password" placeholder="mot de passe..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn">Connecter</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 social-login">
                        	<h3>...</h3>
                        	<div class="social-login-buttons">
	                        	<a class="btn-link-1" href="#"><i class="fa fa-facebook"></i></a>
	                        	<a class="btn-link-1" href="#"><i class="fa fa-twitter"></i></a>
	                        	<a class="btn-link-1" href="#"><i class="fa fa-google-plus"></i></a>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>