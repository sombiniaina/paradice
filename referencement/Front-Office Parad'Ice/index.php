<?php
	require('connexionmysql.php');
	require('model.php');

	$connexion=Mysql_connection();
	$findProduit1=find_table("Produit where idCategorie=1 limit 4", "", $connexion);
	$findProduit2=find_table("Produit where idCategorie=1 order by id desc limit 4", "", $connexion);
	$findProduit3=find_table("Produit where idCategorie=2 limit 4", "", $connexion);
	$findProduit4=find_table("Produit where idCategorie=2 order by id desc limit 4", "", $connexion);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Parad'Ice,de la glace a volonte </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Keywords" content="glace,cornet,pot,fraise,banane,yaourt,malaga,citron,mangue,foret,noir,simple,petit,cafe,coco,chocolat,bon,gout,Madagascar,maison,unique,original">
		<meta name="description" content="Parad'Ice, vente de glace en cornet ou en petit pot avec des divers parfums selon votre choix, avec un prix abordable a partir de 1000 Ar ! ">
		<!--[if ie]><meta content='IE=8' http-equiv='X-UA-Compatible'/><![endif]-->
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">      
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		
		<link href="themes/css/bootstrappage.css" rel="stylesheet"/>
		
		<!-- global styles -->
		<link href="themes/css/flexslider.css" rel="stylesheet"/>
		<link href="themes/css/main.css" rel="stylesheet"/>

		<!-- scripts -->
		<script src="themes/js/jquery-1.7.2.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>				
		<script src="themes/js/superfish.js"></script>	
		<script src="themes/js/jquery.scrolltotop.js"></script>
		<!--[if lt IE 9]>			
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
    <body>		
		
		<div id="wrapper" class="container">
			
			<section  class="homepage-slider" id="home-slider">
				<img src="themes/images/carousel/images0.jpg" alt="Parad'Ice" />
			</section>
			
			<section class="header_text">
				<h1>Parad'Ice</h1> 
				<br/>
				<h2>Parad'Ice, est une entreprise de fabrication de glace a Madagascar.</h2> 
				<br/>
				<h4>Cher client, vous etes a la recherche d'une bonne glace avec toute sorte de parfum, Parad'Ice est la pour vous satisfaire avec ses divers games de produits. Chez parad'Ice vous trouveriez
				des glaces en cornet ou bien des glaces en petit pot avec des prix abordables a partir de 1000 Ar. Chez parad'Ice, on prends des commandes si vous desireriez un autre parfum hors de notre liste. 
				On fait aussi des servir traiteur pour vos toute sorte de type d'evenement, on va vous rafraichir avec des glaces faits maisons mais original avec un gout unique . Merci bien de visiter notre page .</h4>
			</section>
			<section class="main-content">
				<div class="row">
					<div class="span12">													
						<div class="row">
							<div class="span12">
								<h4 class="title">
									<span class="pull-left"><span class="text"><span class="line">Glace <strong> en cornet</strong></span></span></span>
									<span class="pull-right">
										<a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button" href="#myCarousel" data-slide="next"></a>
									</span>
								</h4>
								<div id="myCarousel" class="myCarousel carousel slide">
									<div class="carousel-inner">
										<div class="active item">
											<ul class="thumbnails">
												<?php foreach($findProduit1 AS $prod){ ?>											
												<li class="span3">
												
													<div class="product-box">
														<span class="sale_tag"></span>
														<p><a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php"><img src="<?php echo $prod-> referencementImage ?>" title="<?php echo $prod->nom ?>" alt="<?php echo $prod->description ?> " /></a></p>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php" class="title"><?php echo $prod->nom ?></a><br/>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php" class="category"><?php echo $prod-> image ?></a>
														<p class="price">MGA <?php echo $prod-> prix ?></p>
													</div>
											
												</li>
												<?php }?>
											</ul>
										</div>
										<div class="item">
											<ul class="thumbnails">
											<?php foreach($findProduit2 AS $prod){ ?>
												<li class="span3">
													<div class="product-box">
														<p><a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie)?>.php"><img src="<?php echo $prod-> referencementImage ?>" title="<?php echo $prod->nom ?>" alt="<?php echo $prod->description ?>" /></a></p>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php" class="title"><?php echo $prod->nom ?></a><br/>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie)?>.php" class="category"><?php echo $prod-> image ?></a>
														<p class="price">MGA <?php echo $prod-> prix ?></p>
													</div>
												</li>
												<?php }?>
																																												
											</ul>
										</div>
									</div>							
								</div>
							</div>						
						</div>
						<br/>
						<div class="row">
							<div class="span12">
								<h4 class="title">
									<span class="pull-left"><span class="text"><span class="line">Glace <strong>en pot</strong></span></span></span>
									<span class="pull-right">
										<a class="left button" href="#myCarousel-2" data-slide="prev"></a><a class="right button" href="#myCarousel-2" data-slide="next"></a>
									</span>
								</h4>
								<div id="myCarousel-2" class="myCarousel carousel slide">
									<div class="carousel-inner">
										<div class="active item">
											<ul class="thumbnails">
											<?php foreach($findProduit3 AS $prod){ ?>
												<li class="span3">
													<div class="product-box">
														<p><a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie)?>.php"><img src="<?php echo $prod-> referencementImage ?>" title="<?php echo $prod->nom ?>" alt="<?php echo $prod->description ?>" /></a></p>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie)?>.php" class="title"><?php echo $prod->nom ?></a><br/>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie)?>.php" class="category"><?php echo $prod-> image ?></a>
														<p class="price">MGA <?php echo $prod-> prix ?></p>
													</div>
												</li>
												<?php }?>
																																												
											</ul>
												
										</div>
										<div class="item">
											<ul class="thumbnails">
											<?php foreach($findProduit4 AS $prod){ ?>
												<li class="span3">
													<div class="product-box">
													
														<p><a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php"><img src="<?php echo $prod-> referencementImage ?>" title="<?php echo $prod->nom ?>" alt="<?php echo $prod->description ?>" /></a></p>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie)?>.php" class="title"><?php echo $prod->nom ?></a><br/>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie)?>.php" class="category"><?php echo $prod-> image ?></a>
														<p class="price">MGA <?php echo $prod-> prix ?></p>
													</div>
												</li>
												<?php }?>
																																												
											</ul>
												
										</div>
									</div>							
								</div>
							</div>						
						</div>
					
					</div>				
				</div>
			</section>
			<section class="our_client">
				<h4 class="title"><span class="text"> Nos Partenaires</span></h4>
				<div class="row">					
					<div class="span2">
						<a href="#"><img alt="Kosak" src="themes/images/clients/14.jpg"></a>
					</div>
					<div class="span2">
						<a href="#"><img alt="Leader Price" src="themes/images/clients/35.jpg"></a>
					</div>
					<div class="span2">
						<a href="#"><img alt="Ice Cream" src="themes/images/clients/1.jpg"></a>
					</div>
					<div class="span2">
						<a href="#"><img alt="Coletti" src="themes/images/clients/2.jpg"></a>
					</div>
					<div class="span2">
						<a href="#"><img alt="La Gastronomie Pizza" src="themes/images/clients/3.jpg"></a>
					</div>
					<div class="span2">
						<a href="#"><img alt="Le Glacier" src="themes/images/clients/4.jpg"></a>
					</div>
				</div>
			</section>
			<section id="footer-bar">
				<div class="row">
					<div class="span3">
						<h4>Navigation</h4>
						<ul class="nav">
							<li><a href="./index.php">Acceuil</a></li>  
							<li><a href="././modules.php">Fiche Produit</a></li>
							<li><a href="./contact.php">Contact</a></li>
													
						</ul>					
					</div>
					<div class="span4">
						<h4>Contact</h4>
						<ul class="nav">
							<li>Administrateur: <a href="#">RAKOTOMAMONJY Sombiniaina 000569</a></li>
							<li>Email : <a href="#">sombiniaina.rakotomamonjy@gmail.com</a></li>
							<li>Tel : <a href="#">0332460120</a></li>
						</ul>
					</div>
					<div class="span5">
						<h3 class="logo"><img src="themes/images/paradIce.png" class="site_logo" alt="Parad'Ice"></h3>
						<h5>Pard'Ice est une entreprise de fabrication de glace avec touts types de parfum selon votre desir. Si vous aviez des questions, merci bien de nous contacter ; nous serons ravis de vous aider</h5>
						<br/>
						<span class="social_icons">
							<a class="facebook" href="#">Facebook</a>
							<a class="twitter" href="#">Twitter</a>
							<a class="skype" href="#">Skype</a>
							<a class="vimeo" href="#">Vimeo</a>
						</span>
					</div>					
				</div>	
			</section>
			<section id="copyright">
				<span>2018 &copy; parad'Ice.com </span>
			</section>
		</div>
		<script src="themes/js/common.js"></script>
		<script src="themes/js/jquery.flexslider-min.js"></script>
		<script type="text/javascript">
			$(function() {
				$(document).ready(function() {
					$('.flexslider').flexslider({
						animation: "fade",
						slideshowSpeed: 4000,
						animationSpeed: 600,
						controlNav: false,
						directionNav: true,
						controlsContainer: ".flex-container" // the container that holds the flexslider
					});
				});
			});
		</script>
    </body>
</html>