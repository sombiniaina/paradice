<?php
	$idProduit = $_GET['id'];
	$idCategorie = $_GET['idcat'];
	require('connexionmysql.php');
	require('model.php');
	$connexion=Mysql_connection();
	$findProduit=find_table("Produit where id=".$idProduit."","", $connexion);
	$findProduit1=find_table("Produit limit 1","", $connexion);
	$findProduit0=find_table("Produit limit 3","", $connexion);
	$findProduit3=find_table("Produit where idCategorie=2 limit 3","", $connexion);
	$findProduit2=find_table("Produit order by id desc limit 1","", $connexion);
	$findCategorie=find_table("Categorie","", $connexion);
	$findParfum=find_table("Parfum","", $connexion);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Parad'Ice,de la glace a volonte </title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="Keywords" content="glace,cornet,pot,fraise,banane,yaourt,malaga,citron,mangue,foret,noir,simple,petit,cafe,coco,chocolat,bon,gout,Madagascar,maison,unique,original">
		<meta name="description" content="Parad'Ice, vente de glace en cornet ou en petit pot avec des divers parfums selon votre choix, avec un prix abordable a partir de 1000 Ar ! ">
		<!--[if ie]><meta content='IE=8' http-equiv='X-UA-Compatible'/><![endif]-->
		
		<!-- bootstrap -->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">      
		<link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">		
		<link href="themes/css/bootstrappage.css" rel="stylesheet"/>
		
		<!-- global styles -->
		<link href="themes/css/main.css" rel="stylesheet"/>
		<link href="themes/css/jquery.fancybox.css" rel="stylesheet"/>
				
		<!-- scripts -->
		<script src="themes/js/jquery-1.7.2.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>				
		<script src="themes/js/superfish.js"></script>	
		<script src="themes/js/jquery.scrolltotop.js"></script>
		<script src="themes/js/jquery.fancybox.js"></script>
		<!--[if lt IE 9]>			
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
	</head>
    <body>
	
		
		<div id="wrapper" class="container">
			<section  class="homepage-slider" id="home-slider">
				<img src="themes/images/carousel/images0.jpg" alt="Parad'Ice" />
			</section>	
			<section class="header_text sub">
			
				<h4><span>Fiche Produit</span></h4>
			</section>
			<section class="main-content">				
				<div class="row">						
					<div class="span9">
					<?php foreach($findProduit AS $prod){ ?>
						<div class="row">
							<div class="span4">
								<a href="" class="thumbnail" data-fancybox-group="group1" title="Description 1"><img src="<?php echo $prod-> referencementImage ?>" title="<?php echo $prod->nom ?>" alt="<?php echo $prod->description ?>" /></a>												
								
							</div>
							<div class="span5">
								<address>
									<strong>Nom :</strong> <span><?php echo $prod->nom ;?></span><br>
									<strong>Code produit:</strong> glace 000<span><?php echo $prod->id ;?></span><br>
									<strong>Code categorie:</strong>000 <span><?php echo $prod->idCategorie ;?></span><br>
									<strong>Categorie Produit:</strong> <span><?php echo $prod->image ;?></span><br>								
								</address>									
								<h4><strong>Prix: MGA <?php echo $prod->prix ;?></strong></h4>
							</div>
												
						</div>
						<?php }?>
						<div class="row">
							<div class="span9">
								<ul class="nav nav-tabs" id="myTab">
									<li class="active"><a href="#home">Description</a></li>
									<li class=""><a href="#profile">Autre Information</a></li>
								</ul>							 
								<div class="tab-content">
									<div class="tab-pane active" id="home"><?php echo $prod->description ;?></div>
									<div class="tab-pane" id="profile">
										<table class="table table-striped shop_attributes">
											<tbody>
												<tr class="">
													<th>Categorie disponible</th>
												</tr>
												<?php foreach($findCategorie AS $cat){ ?>
												<tr class="">
												    <td><?php echo $cat-> typeCategorie ?></td>
												</tr>
												<?php }?>
											</tbody>
										</table>
									</div>
								</div>							
							</div>						
							<div class="span9">	
								<br>
								<h4 class="title">
									<span class="pull-left"><span class="text"><strong>	Autres</strong> Propositions</span></span>
									<span class="pull-right">
										<a class="left button" href="#myCarousel-1" data-slide="prev"></a><a class="right button" href="#myCarousel-1" data-slide="next"></a>
									</span>
								</h4>
								<div id="myCarousel-1" class="carousel slide">
									<div class="carousel-inner">
										<div class="active item">
											<div class="active item">
											<ul class="thumbnails">
												<?php foreach($findProduit0 AS $prod){ ?>											
												<li class="span3">
												
													<div class="product-box">
														<span class="sale_tag"></span>
														<p><a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php"><img src="<?php echo $prod-> referencementImage ?>" title="<?php echo $prod->nom ?>" alt="<?php echo $prod->description ?>" /></a></p>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php" class="title"><?php echo $prod->nom ?></a><br/>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php" class="category"><?php echo $prod-> image ?></a>
														<p class="price">MGA <?php echo $prod-> prix ?></p>
													</div>
											
												</li>
												<?php }?>
											</ul>
										</div>
										</div>
										<div class="item">
											<div class="active item">
											<ul class="thumbnails">
												<?php foreach($findProduit3 AS $prod){ ?>											
												<li class="span3">
												
													<div class="product-box">
														<span class="sale_tag"></span>
														<p><a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php"><img src="<?php echo $prod-> referencementImage ?>" title="<?php echo $prod->nom ?>" alt="<?php echo $prod->description ?>" /></a></p>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php" class="title"><?php echo $prod->nom ?></a><br/>
														<a href="<?php echo splitter($prod->nom) ?>-<?php echo splitter($prod->description) ?>-<?php echo splitter($prod->id) ?>-<?php echo splitter($prod->idCategorie) ?>.php" class="category"><?php echo $prod-> image ?></a>
														<p class="price">MGA <?php echo $prod-> prix ?></p>
													</div>
											
												</li>
												<?php }?>
											</ul>
										</div>   
												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="span3 col">
						<div class="block">	
							<ul class="nav nav-list">
								<li class="active"><a href="#">LES PARFUMS DISPONIBLES</a></li>
								<?php foreach($findParfum AS $parf){ ?>
								    <li><?php echo $parf->typeParfum ?></li>
								<?php }?>
							</ul>
							<br/>
							
						</div>
						<div class="block">
							<h4 class="title">
								<span class="pull-left"><span class="text">EXEMPLES DE PRODUITS</span></span>
								<span class="pull-right">
									<a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button" href="#myCarousel" data-slide="next"></a>
								</span>
							</h4>
							<div id="myCarousel" class="carousel slide">
								<div class="carousel-inner">
									<div class="active item">
										<ul class="thumbnails listing-products">
											<li class="span3">
											<?php foreach($findProduit1 AS $prod1){ ?>
												<div class="product-box">
													<span class="sale_tag"></span>												
													<a href="#"><img alt="<?php echo $prod1-> description ?>" src="<?php echo $prod1->referencementImage ?>" title="<?php echo $prod1->nom ?>" ></a><br/>
													<a href="#" class="title"><?php echo $prod1-> nom ?></a><br/>
													<a href="#" class="category"><?php echo $prod1-> image ?></a>
													<p class="price">MGA <?php echo $prod1-> prix ?></p>
												</div>
											<?php }?>
											</li>
										</ul>
									</div>
									<div class="item">
										<ul class="thumbnails listing-products">
											<?php foreach($findProduit2 AS $prod2){ ?>
												<div class="product-box">
													<span class="sale_tag"></span>												
													<a href="#"><img alt="<?php echo $prod2-> description ?>" src="<?php echo $prod2->referencementImage ?>" title="<?php echo $prod2->nom ?>"></a><br/>
													<a href="#" class="title"><?php echo $prod2-> nom ?></a><br/>
													<a href="#" class="category"><?php echo $prod2-> image ?></a>
													<p class="price">MGA <?php echo $prod2-> prix ?></p>
												</div>
											<?php }?>
										</ul>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</section>			
			<section id="footer-bar">
				<div class="row">
					<div class="span3">
						<h4>Navigation</h4>
						<ul class="nav">
							<li><a href="./index.php">Acceuil</a></li>  
							<li><a href="././modules.php">Fiche Produit</a></li>
							<li><a href="./contact.php">Contact</a></li>
													
						</ul>					
					</div>
					<div class="span4">
						<h4>Contact</h4>
						<ul class="nav">
							<li>Administrateur: <a href="#">RAKOTOMAMONJY Sombiniaina 000569</a></li>
							<li>Email : <a href="#">sombiniaina.rakotomamonjy@gmail.com</a></li>
							<li>Tel : <a href="#">0332460120</a></li>
						</ul>
					</div>
					<div class="span5">
						<h4 class="logo"><img src="themes/images/paradIce.png" class="site_logo" alt="Parad'Ice"></h4>
						<h5>Pard'Ice est une entreprise de fabrication de glace avec touts types de parfum selon votre desir. Si vous aviez des questions, merci bien de nous contacter ; nous serons ravis de vous aider</h5>
						<br/>
						<span class="social_icons">
							<a class="facebook" href="#">Facebook</a>
							<a class="twitter" href="#">Twitter</a>
							<a class="skype" href="#">Skype</a>
							<a class="vimeo" href="#">Vimeo</a>
						</span>
					</div>					
				</div>	
			</section>
			<section id="copyright">
				<span>2018 &copy; parad'Ice.com </span>
			</section>
		</div>
		<script src="themes/js/common.js"></script>
		<script>
			$(function () {
				$('#myTab a:first').tab('show');
				$('#myTab a').click(function (e) {
					e.preventDefault();
					$(this).tab('show');
				})
			})
			$(document).ready(function() {
				$('.thumbnail').fancybox({
					openEffect  : 'none',
					closeEffect : 'none'
				});
				
				$('#myCarousel-2').carousel({
                    interval: 2500
                });								
			});
		</script>
    </body>
</html>