create database ParadIce;
use ParadIce;


create table Utilisateur(
	id int unsigned NOT NULL auto_increment,
	nom varchar(20),
	prenom varchar(20),
	mail varchar(20),
	password varchar(100),
	primary key(id)
);

insert into Utilisateur(nom,prenom,mail,password) values('Rakotomamonjy','Sombiniaina','Sombiniaina@gmail.com',sha1('itu'));

create table Categorie(
	id int unsigned NOT NULL auto_increment, 
	typeCategorie varchar(50),
	primary key (id)
);

insert into Categorie(typeCategorie) values('cornet');
insert into Categorie(typeCategorie) values('pot');
insert into Categorie(typeCategorie) values('barket');

create table Parfum(
	id int unsigned NOT NULL auto_increment, 
	typeParfum varchar(50),
	primary key (id)
);

insert into Parfum(typeParfum) values('fraise');
insert into Parfum(typeParfum) values('chocolat');
insert into Parfum(typeParfum) values('coco');
insert into Parfum(typeParfum) values('mangue');
insert into Parfum(typeParfum) values('citron');
insert into Parfum(typeParfum) values('banane');
insert into Parfum(typeParfum) values('malaga');
insert into Parfum(typeParfum) values('foret noir');
insert into Parfum(typeParfum) values('goyave');
insert into Parfum(typeParfum) values('yaourt');
insert into Parfum(typeParfum) values('biscuit');
insert into Parfum(typeParfum) values('pomme');
insert into Parfum(typeParfum) values('abricot');


create table Produit(
	id int unsigned NOT NULL auto_increment, 
	idCategorie int,
	idParfum int,
	nom varchar(50),
	description varchar(50),
	prix int,
	referencementImage varchar(200),
	image varchar(50),
	primary key (id),
	foreign key(idCategorie) references Categorie(id),
	foreign key(idParfum) references Parfum(id)
);
/* en cornet*/	
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,1,'glace a la fraise','glace en cornet simple a la fraise',1000,'themes/images/ladies/f1.jpg','cornet simple');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,2,'glace au chocolat','glace en cornet simple au chocolat',1000,'themes/images/ladies/f4.jpg','cornet simple');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,3,'glace au coco','glace en cornet simple au parfum coco',1000,'themes/images/ladies/f2.jpg','cornet simple');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,4,'glace a la mangue','glace en cornet simple a la mangue',1000,'themes/images/ladies/f3.jpg','cornet simple');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,5,'glace au citron','glace en cornet simple au citron',1000,'themes/images/ladies/f7.jpg','cornet simple');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,6,'glace au banane','glace en cornet simple au banane',1000,'themes/images/ladies/f6.jpg','cornet simple');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,7,'glace malaga','glace en cornet simple au parfum malaga',1000,'themes/images/ladies/f5.jpg','cornet simple');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(1,8,'glace au foret noir','glace en cornet simple au foret noir',1000,'themes/images/ladies/f4.jpg','cornet simple');

/* en pot */
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(2,8,'glace a la fraise','glace en petit pot simple au parfum fraise',2000,'themes/images/ladies/f8.jpg','petit pot');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(2,5,'glace au citron','glace en petit pot au citron',2000,'themes/images/ladies/f10.jpg','petit pot');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(2,7,'glace malaga','glace en petit pot au parfum malaga',2000,'themes/images/ladies/f13.jpg','petit pot');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(2,3,'glace au coco','glace en petit pot au parfum coco',2000,'themes/images/ladies/f9.jpg','petit pot');
insert into Produit(idCategorie,idParfum,nom,description,prix,referencementImage,image) values(2,10,'glace au yaourt','glace en petit pot au yaourt',2000,'themes/images/ladies/f12.jpg','petit pot');